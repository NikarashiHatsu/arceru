<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arceru - Login</title>
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/bootstrap-grid.css">
</head>
<body>
    <div class="container-fluid px-0 pr-3 auth">
        <div class="row">
            <div class="col-6 d-flex justify-items-center align-items-center auth-sidebar">
                <img src="./img/login-register-sidebar.png" alt="Sidebar" class="auth-sidebar-image">
                <h2 class="auth-sidebar-text my-0">Arceru</h2>
            </div>
            <div class="col-6 justify-content-center align-items-center auth-form">
                <div class="row py-5">
                    <div class="offset-2 col-8">
                        <h4 class="text-center mb-3">Register to join the realm.</h4>
                        <!-- Alert
                            <div class="alert alert-info mb-3">
                                <p>Halo Player!</p>
                                <p>Kamu telah mendaftarkan akun di Arceru. Untuk memverifikasi alamat email, silahkan klik link <a href="javascript:void(0);">ini</a>.</p>
                            </div>
                        -->
                        <form action="" method="post">
                            <div class="form-input mb-3">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" placeholder="Nama yang digunakan ketika bermain" />
                            </div>
                            <div class="form-input mb-3">
                                <label for="email">Alamat Email</label>
                                <input type="text" name="email" id="email" placeholder="Email hanya digunakan untuk keperluan keamanan" />
                            </div>
                            <div class="form-input mb-3">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" placeholder="Password yang digunakan ketika bermain" />
                            </div>
                            <div class="form-input mb-5">
                                <label for="confirm-password">Ulangi password</label>
                                <input type="password" name="confirm-password" id="confirmPassword" placeholder="Ulangi Password" />
                            </div>
                            <div>
                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                                <p class="text-center">Already have an account? Why don't you <a href="./login.php">login</a> instead?</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./js/input.js"></script>
</body>
</html>