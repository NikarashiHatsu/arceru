<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arceru - Login</title>
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/bootstrap-grid.css">
</head>
<body>
    <div class="container-fluid px-0 pr-3 auth">
        <div class="row">
            <div class="col-6 d-flex justify-items-center align-items-center auth-sidebar">
                <img src="./img/login-register-sidebar.png" alt="Sidebar" class="auth-sidebar-image">
                <h2 class="auth-sidebar-text my-0">Arceru</h2>
            </div>
            <div class="col-6 justify-content-center align-items-center auth-form">
                <div class="row py-5">
                    <div class="offset-2 col-8">
                        <h4 class="text-center mb-3">Login to rule over your account.</h4>
                        <!-- Alert
                            <div class="alert alert-danger mb-3">
                                <p>Akun tidak terdaftar atau password salah.</p>
                            </div>
                        -->
                        <form action="" method="post">
                            <div class="form-input mb-3">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" placeholder="Nama yang digunakan ketika bermain" />
                            </div>
                            <div class="form-input mb-5">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" placeholder="Password yang digunakan ketika bermain" />
                            </div>
                            <div>
                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                </div>
                                <p class="text-center">Don't have an account? Consider to <a href="./register.php">register.</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>