const input = document.querySelectorAll( 'input' ).forEach( item => {

    item.addEventListener( 'keyup', event => {

        const value = item.value.trim();
        const label = item.parentElement.querySelector( 'label' );

        if( value != "" ) {
            label.classList.add( 'filled' );
        } else {
            label.classList.remove( 'filled' );
        }
        
    });

});